# gitlab_proof

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs

[Verifying my OpenPGP key: openpgp4fpr:878E742048DEBC7BDE3E6D3A2409909AABDCDE3B]
